﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections;

public class FillData : MonoBehaviour {
    public Slider slider;
    public string[] txtQuestions;
    public TextMeshProUGUI textQuestion;
    public Image[] elementButtons;
    public GameObject nextButton;
    public GameObject checkIcon;
    public Text bubbleText;
    public TextMeshProUGUI textTopic, textTag;
    public SliderEvent slideEvt;
    int[] allData = new int[] { 0, 0, 0, 0, 0, 0};
    int currentSelectedID;
    string[] buttonsValue;
    Global global;
    public Image showSelected;

    void Start() {
        global = Global.ins;
        SetElement(0);
        slideEvt.EndDrag += UpdateCurrentSliderValue;
        buttonsValue = new string[elementButtons.Length];
        int i = 0;
        foreach(Image v in elementButtons) {
            buttonsValue[i] = v.transform.GetComponentInChildren<TextMeshProUGUI>().text;
            i++;
        }
        textTopic.text = buttonsValue[0];
        StartCoroutine("AutoUpdateSlider");
    }

    IEnumerator AutoUpdateSlider() {
        allData[currentSelectedID] = (int)slider.value;
        bubbleText.text = "" + slider.value;
        allData[currentSelectedID] = (int)slider.value;
        if (allData[currentSelectedID] != 0)
            checkIcon.SetActive(true);
        CheckVisibleNextButton();
        yield return new WaitForSeconds(0.05f);
        StartCoroutine("AutoUpdateSlider");
    }

    private void UpdateCurrentSliderValue(float val) {
        allData[currentSelectedID] = (int)val;
    }

    public void OnClickButtonGroup(int id) {
        checkIcon.SetActive(false);
        if (allData[id] != 0) checkIcon.SetActive(true);
        Global.ins.PlaySoundIndex(0);
        for (int i = 0; i < elementButtons.Length; i++) {
            if(i == id) {
                elementButtons[i].transform.GetChild(1).gameObject.SetActive(true);
                showSelected.sprite = elementButtons[i].transform.GetChild(1).GetComponent<Image>().sprite;
                
            } else {
                elementButtons[i].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        textTopic.text = buttonsValue[id];
        SetElement(id);
        textQuestion.text = txtQuestions[id];
    }

    public void AddDecreaseSlider(bool isAdd) {
        slider.value = (isAdd) ? (slider.value += 1): (slider.value -= 1);
        UpdateSlider();
        allData[currentSelectedID] = (int)slider.value;
    }

    void SetElement(int id) {
        slider.gameObject.SetActive(false);
        switch (id) {
            case 0: //age
                slider.minValue = 0;
                slider.maxValue = 90;                
                textTag.text = "อายุ/ปี";
                break;
            case 1: //weight
                slider.minValue = 0;
                slider.maxValue = 130;
                textTag.text = "น้ำหนัก/กิโลกรัม";
                break;
            case 2: //Height
                slider.minValue = 0;
                slider.maxValue = 230;
                textTag.text = "ส่วนสูง/เซนติเมตร";
                break;
            case 3: //Sleep
                slider.minValue = 0;
                slider.maxValue = 20;
                textTag.text = "ชั่วโมง/วัน";
                break;
            case 4: //Drink
                slider.minValue = 0;
                slider.maxValue = 20;
                textTag.text = "แก้ว/วัน";
                break;
            case 5: //Fitness
                slider.minValue = 0;
                slider.maxValue = 90;
                textTag.text = "ชั่วโมง/สัปดาห์";
                break;
        }
        
        slider.gameObject.SetActive(true);       
        currentSelectedID = id;
        SetDefaultSliderValue();
    }

    private void SetDefaultSliderValue() {
        slider.value = allData[currentSelectedID];
        /*
        if (allData[currentSelectedID] == 0) {
            slider.value = 0;
            allData[currentSelectedID] = (int)slider.value;
        } else {
        //    print(slider.minValue + " min <> max " + slider.maxValue);
        //    print(allData[currentSelectedID]);
            slider.value = allData[currentSelectedID];
            
        }*/
        bubbleText.text = "" + slider.value;
        CheckVisibleNextButton();
    }

    private void CheckVisibleNextButton() {
        for(int i = 0; i < allData.Length; i++) {
            print(" i = " + i + " : value = " + allData[i]);
            nextButton.SetActive(false);
            if (allData[i] == 0) return;
        }/*
        foreach(int i in allData) {
            print(" i = " + i + " : value = " + allData[i]);
            if (i == 0) return;
        }*/
        nextButton.SetActive(true);
    }

    public void UpdateSlider() {      
        DOTween.KillAll(); //Kill data not in use.
        DOVirtual.DelayedCall(.05f, () => { //Force to Unity's not auto update slider data by itself.
            //Real update data.
            bubbleText.text = "" + slider.value;
            allData[currentSelectedID] = (int)slider.value;
            if (allData[currentSelectedID] != 0) 
                checkIcon.SetActive(true);
            CheckVisibleNextButton();
        });
       
    }

    public void OnClickNext() {
        Global.ins.PlaySoundIndex(0);
        global.age = allData[0];
        global.weight = allData[1];
        global.height = allData[2];
        global.sleep = allData[3];
        global.drink = allData[4];
        global.fitness = allData[5];

        print("age > " + global.age);
        print("weight "+ global.weight);
        print("height "+ global.height);
        print("sleep "+ global.sleep);
        print("drink "+ global.drink);
        print("fitness " + global.fitness);

        global.OnLoadSceneAt(3);
    }

}
