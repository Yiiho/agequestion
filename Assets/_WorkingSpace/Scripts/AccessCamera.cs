﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AccessCamera : MonoBehaviour {
    WebCamTexture camTexture;
    public GameObject guideline;
    void Start() {
        if (camTexture == null) {
            camTexture = new WebCamTexture(WebCamTexture.devices[0].name, 1920, 1080, 60);
        }
        GetComponent<RawImage>().texture = camTexture;
        OnActiveCamera(true);
    }

    public void OnActiveCamera(bool isActive) {
        if (isActive) {
            if (!camTexture.isPlaying) {
                camTexture.Play();
            }
        } else {/*
            if (camTexture.isPlaying) {
                print("Innnnnnnnnnnnnnnnnnnnnnnnnnnnnn EndDeactiveCamera");
                camTexture.Stop();
            }else if(camTextu)
            print("EndDeactiveCamera");
            */
            camTexture.Stop();
        }
    }

    public void OnStartCapture(int countTime) {
        StartCoroutine(StartCapture(countTime));
    }

    IEnumerator StartCapture(int cTime) {
        yield return new WaitForSeconds(1.0f);
        if (cTime - 1 == 0) {
            OnCapture();
        } else {
            StartCoroutine(StartCapture(cTime-=1));
        }
        //print(cTime);
    }

    private void OnCapture() {
        guideline.SetActive(false);
        camTexture.Pause();
    }

    public void OnResumeCam() {
        camTexture.Play();
    }
}