﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Home : MonoBehaviour
{

    public void OnPressNext() {
        SceneManager.LoadScene(2);
        Global.ins.PlaySoundIndex(0);
    }
}
