﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System;

public class Capture : MonoBehaviour {
    public GameObject[] groupPanel;
    public AccessCamera cam;
    public Transform genderToggleGroup;
    public CalculateInfo calculateInfo;
    Text countdownTxt;
    ScreenCapture screenCapture;
    Dictionary<string, string> userData = new Dictionary<string, string>();


    void Start() {
        foreach(GameObject go in groupPanel) {
            go.SetActive(false);
        }
        groupPanel[0].SetActive(true);
        screenCapture = GetComponent<ScreenCapture>();
        string path = Application.dataPath + "/../" + "Screenshots/";
        screenCapture.outputFolder = path;
        InitUserData();
    }

    private void InitUserData() {
        userData.Add("Age", "");
        userData.Add("Height", "");
        userData.Add("Weight", "");
        userData.Add("Gender", "");
    }

    #region Fill data page.
    public void GoToCapturePanel() {
        InputField[] inputs = 
            groupPanel[0].transform.GetComponentsInChildren<InputField>();
        print(inputs.Length);
        foreach(InputField ip in inputs) {
            userData[ip.name] = ip.text;
            if (ip.text == "") return;
        }

        Toggle[] toggles = genderToggleGroup.GetComponentsInChildren<Toggle>();
        foreach(Toggle t in toggles) {
            if (t.isOn) {
                userData["Gender"] = t.name;
                break;
            }
        }

        print(userData["Age"]);
        print(userData["Gender"]);
        groupPanel[0].transform.DOMoveX(-1920, 1).OnComplete(OpenTakeImagePanel);
        groupPanel[1].SetActive(true);


        ActiveObject(humanGuideline);
        calculateInfo.FadeGuideline(.8f);
        cam.OnActiveCamera(true);
    }
    #endregion

    #region Take photo page
    GameObject tmbBtn;
    public void OnHideButton(GameObject btn) {
        tmbBtn = btn;
        tmbBtn.SetActive(false);
    }

    private void OnActiveTmbButton() {
        if (tmbBtn != null) {
            tmbBtn.SetActive(true);
            tmbBtn = null;
        }
    }

    public void OnUseImage(bool isUse) {
        retakeGroup.SetActive(false);
        retakeGroup = null;
        if (isUse) {
            screenCapture.TakeScreenShot();
            GotoQuestionPage();
        } else {
            OnActiveTmbButton();
        }
    }

    void OpenTakeImagePanel() {
        groupPanel[0].SetActive(false);
    }

    public void OnPressCapture() {
        cam.OnStartCapture(5);
        StartCount();
    }

    int count;
    void StartCount() {
        if (countdownTxt == null) countdownTxt =
            groupPanel[1].transform.Find("CountdownTxt").GetComponent<Text>();
        count = 5;
        countdownTxt.text = count.ToString();
        UpdateCountText();
    }
    int textSize = 300;
    void UpdateCountText() {
        textSize = 300;
        count--;
        if (count > 0) {
            countdownTxt.text = count.ToString();
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(UpdateCountText);
        } else {
            countdownTxt.text = "SNAP";
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(ShowRetakePanel);
        }
    }
    GameObject retakeGroup;
    private void ShowRetakePanel() {
        AddTakePhotoEffect();

        countdownTxt.text = "";
        if (retakeGroup==null) 
            retakeGroup = groupPanel[1].transform.Find("RetakeGroup").gameObject;
        retakeGroup.gameObject.SetActive(true);
    }

    void UpdateTextSize() {
        countdownTxt.fontSize = textSize;
    }
    #endregion

    #region Question page
    private void GotoQuestionPage() {
        groupPanel[1].transform.DOMoveX(-1920, 1).OnComplete(()=> 
            calculateInfo.InitCalculate(userData));
        groupPanel[2].SetActive(true);
    }

    #endregion


    #region Method Display2

    public GameObject humanGuideline;
    public Image fadeEffect;
    public void ActiveObject(GameObject go) {
        go.SetActive(true);
    }

    public void DeactiveObject(GameObject go) {
        go.SetActive(false);
    }

    private void AddTakePhotoEffect() {
        fadeEffect.color = Color.white;
        fadeEffect.DOFade(0, .3f);
    }
    #endregion

}
