﻿using System.IO;
using UnityEngine;
using SimpleJSON;

public class LoadConfigFile : MonoBehaviour {
    [HideInInspector]
    public string path;
    private void Start() {
        path = Application.dataPath + "/../Files/Configure.json";
        GetLoadInterval();
    }

    private void GetLoadInterval() {        
        string s = File.ReadAllText(path);
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        int interval = int.Parse(jsonNode["IntervalTime"]);
        Config conf = new Config(interval);
        conf._interval = interval;
        Global.ins.OnLoadConfigCompleted(conf);
    }
}

[System.Serializable]
public class Config {
    public int _interval;
    public Config(int interval) {
        _interval = interval;
    }
}

