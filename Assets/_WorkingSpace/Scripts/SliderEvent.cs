﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderEvent : MonoBehaviour, IPointerUpHandler {
    public delegate void EndSliderDragEventHandler(float val);
    public event EndSliderDragEventHandler EndDrag;
    Slider slider;

    void Start() {
        slider = GetComponent<Slider>();
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (EndDrag != null) {
            EndDrag(slider.value);
        }
    }

}
