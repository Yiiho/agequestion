﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Global : MonoBehaviour{
    public static Global ins;
    public int age, weight, height, drink, sleep, fitness;

    public AudioClip[] audClips;
    public AudioSource audS;

    private void Awake() {
        ins = this;
        
    }

    private void Start() {
        DontDestroyOnLoad(this.gameObject);
        Invoke("ActiveAllMornitors", .5f);
    }

    void ActiveAllMornitors() {
        if (Display.displays.Length > 1) {
            Display.displays[1].Activate(0, 0, 60);
        }
        // Cursor.visible = false;
        Screen.fullScreen = true;
    }

    public void OnLoadSceneAt(int index) {
        SceneManager.LoadScene(index);
    }

    public string ProfilePath {
        get; set;
    }

    public Sprite ProfileImage {
        get; set;
    }

    public void PlaySoundIndex(int id) {
        audS.clip = audClips[id];
        audS.Play();
    }

    public int interval = 0;
    public void OnLoadConfigCompleted(Config conf) {
        interval = conf._interval;
        OnLoadSceneAt(1);
    }
    
}
