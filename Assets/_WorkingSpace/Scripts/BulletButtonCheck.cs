using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletButtonCheck : MonoBehaviour
{
    public Image mainTabBG;
    public GameObject[] layerToggle;
    public Sprite[] allMainTabBG;
    public Result result;

    public void OnToggleId(int id) {
        Global.ins.PlaySoundIndex(0);
        mainTabBG.sprite = allMainTabBG[id];
        result.ToggleDataManual(id);
        for (int i = 0; i < layerToggle.Length; i++) {
            if (id == i) {
                layerToggle[i].SetActive(true);                
            } else {
                layerToggle[i].SetActive(false);
            }
        }
    }

}
