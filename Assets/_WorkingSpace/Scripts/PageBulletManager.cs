﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PageBulletManager : MonoBehaviour {
    public int maxDataPage = 4;
    public Transform[] bulletsFixed;
    public Transform bulletMove;
    public Button[] buttons;
    
    public void SetBulletId(int id) {
        if(id >=0 && id < bulletsFixed.Length){
            bulletMove.localPosition = bulletsFixed[id].localPosition;
        }
        SetButtonVisible(id);
    }

    private void SetButtonVisible(int id) {
        switch(id){
            case 0:
                DeactiveButton(new Button[]{ buttons[1] });
                break;
            case 3:
                DeactiveButton(new Button[]{ buttons[0], buttons[2] });
                break;
            default:
                DeactiveButton(new Button[]{ buttons[0], buttons[1] });
                break;
        }
    }

    private void DeactiveButton(Button[] btns) {
        foreach(Button btn in buttons) {
            bool isSame = false;
            foreach(Button _btn in btns) {
                if(btn == _btn) {
                    isSame = true;
                    break;
                }
            }
            if(btn.gameObject.activeSelf) {
                if(!isSame) {
                    btn.image.DOFade(0f, .05f).OnComplete(()=>
                        btn.gameObject.SetActive(false));
                }
            }
        }
        DOVirtual.DelayedCall(.15f, ()=> ActiveButton(btns));        
    }

    private void ActiveButton(Button[] btns) {
        foreach(Button btn in btns) {
            btn.gameObject.SetActive(true);
            btn.image.DOFade(1f, .05f).SetDelay(.05f);
        }
    }
}
