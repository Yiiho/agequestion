﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class CalculateInfo : MonoBehaviour {
    public Text heartTxt, liverTxt, intestinTxt;
    public Image heartImg, liverImg, intestinImg;
    public GameObject[] infoGroup;
    public Image guideLine;

    Dictionary<string, string> userData;

    void Start() {
        
    }

    public void FadeGuideline(float value) {
        guideLine.DOFade(value, .25f);
    }

    public void InitCalculate(Dictionary<string, string> _userData) {
        FadeGuideline(0f);
        userData = _userData;
        heartImg.DOFade(1, 0.2f).OnComplete(() => 
            heartImg.transform.GetChild(0).gameObject.SetActive(true));
        liverImg.DOFade(1, 0.2f).SetDelay(.2f).OnComplete(() =>
            liverImg.transform.GetChild(0).gameObject.SetActive(true));
        intestinImg.DOFade(1, 0.2f).SetDelay(.4f).OnComplete(() =>
            intestinImg.transform.GetChild(0).gameObject.SetActive(true));
        guideLine.DOFade(0, .2f);
    }

    public void OnToggleGroup(GameObject go) {
        foreach(GameObject _go in infoGroup) {
            if(_go == go) {
                _go.SetActive(true);
            } else {
                _go.SetActive(false);
            }
        }
    }
}
