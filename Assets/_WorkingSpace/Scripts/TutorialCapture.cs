﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TutorialCapture : MonoBehaviour {
    public AccessCamera cam;
    public Text countdownTxt, countdownTxt2;
    public GameObject tmbCountDownGroup;
    public GameObject retakeGroup;
    ScreenCapture screenCapture;
    public GameObject tutorLayer, fillDataLayer;

    void Start() {
        screenCapture = GetComponent<ScreenCapture>();
        string path = Application.dataPath + "/../" + "Screenshots/";
        screenCapture.outputFolder = path;
        retakeGroup.SetActive(false);
        tmbCountDownGroup.SetActive(false);
    }

    #region Fill data page.
    public void GoToCapturePanel() {
        ActiveObject(humanGuideline);
        cam.OnActiveCamera(true);
    }
    #endregion

    #region Take photo page
    GameObject tmbBtn;
    public void OnHideButton(GameObject btn) {
        tmbBtn = btn;
        tmbBtn.SetActive(false);
    }

    private void OnActiveTmbButton() {
        if (tmbBtn != null) {
            tmbBtn.SetActive(true);
            tmbBtn = null;
        }
    }

    public void OnUseImage(bool isUse) {
        retakeGroup.SetActive(false);
        Global.ins.PlaySoundIndex(0);
        if (isUse) {
            retakeGroup = null;
            screenCapture.TakeScreenShot();
            tutorLayer.SetActive(false);
            fillDataLayer.SetActive(true);
            cam.OnActiveCamera(false);
            menu_Select.SetActive(true);
        } else {
            ActiveObject(humanGuideline);
            OnActiveTmbButton();
            cam.OnResumeCam();
            if (_button!=null) {
                _button.gameObject.SetActive(true);
                _button.image.color = Color.white;
            }
        }
    }

    Button _button;
    public void OnPressCapture(Button button=null) {
        if (button) {
            Global.ins.PlaySoundIndex(0);
            _button = button;
            if (button.image.color.a != 1) return;
            button.image.DOFade(0, .3f).OnComplete(() => {
                button.gameObject.SetActive(false);
                tmbCountDownGroup.SetActive(true);
            });
        }
        cam.OnStartCapture(5);
        StartCount();
    }

    int count;
    void StartCount() {
        count = 6;
        countdownTxt.text = countdownTxt2.text = count.ToString();
        UpdateCountText();
    }
    int textSize = 300;
    void UpdateCountText() {
        
        textSize = 300;
        count--;
        if (count > 0) {
            Global.ins.PlaySoundIndex(1);
            countdownTxt.text = countdownTxt2.text = count.ToString();
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(UpdateCountText);
        } else {
            Global.ins.PlaySoundIndex(2);
            countdownTxt.text = countdownTxt2.text = "CAPTURE";
            DOTween.To(() => textSize, x => textSize = x, 150, 1f).OnUpdate(UpdateTextSize).OnComplete(ShowRetakePanel);
        }
    }
   
    private void ShowRetakePanel() {
        Global.ins.PlaySoundIndex(3);
        AddTakePhotoEffect();

        countdownTxt.text = countdownTxt2.text = "";
        tmbCountDownGroup.SetActive(false);
        retakeGroup.SetActive(true);
    }

    void UpdateTextSize() {
        countdownTxt.fontSize = countdownTxt2.fontSize = textSize;
    }
    #endregion

    #region Method Display2

    public GameObject humanGuideline;
    public Image fadeEffect;
    public GameObject menu_Select;
    public void ActiveObject(GameObject go) {
        go.SetActive(true);
    }

    public void DeactiveObject(GameObject go) {
        go.SetActive(false);
    }

    private void AddTakePhotoEffect() {
        fadeEffect.color = Color.white;
        fadeEffect.DOFade(0, .3f);
    }
    #endregion

    float timer = 0;
    private void Update() {
        if (Input.GetMouseButton(0)) {
            timer = 0;
        }
        timer += Time.deltaTime;
        if (timer >= Global.ins.interval) {
            cam.OnActiveCamera(false);
            Global.ins.OnLoadSceneAt(1);
        }
    }
}
