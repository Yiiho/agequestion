using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.IO;

public class Result : MonoBehaviour {

    
    public Image raw_ans;
    
    public Sprite[] result_spi_List;
    public Sprite[] drink_spi_List;
    public Sprite[] sleep_spi_List;
    public Sprite[] bmi_spi_List;

    public Image mainTabIcon;
    public TextMeshProUGUI headerTxt, dataTxt;
    public Image profileImg, profileImg2;

    public Image d2Icon;
    public Sprite[] drinkSpr, sleepSpr, bmiSpr;
    public GameObject[] fx;
    public TextMeshProUGUI d2Text, d2Text_data;

    public Transform water;
    public Image bmiImage;
    public Sprite[] bmiList;
    Sprite drinkUsing, sleepUsing, bmiUsing;

    //float drinkMass;
    Global global;

    string[] headerResultText = new string[4];
    string[] dataResultText = new string[4];

    int currentIndex = 0;

    void Start() {
        global = Global.ins;

        ProcessDrink();
        ProcessSleep();
        ProcessBodyMass();
        ProcessResult();

        LoadDataPage();
        d2Icon.sprite = mainTabIcon.sprite = drinkUsing;
        profileImg.sprite = profileImg2.sprite = LoadNewSprite(global.ProfilePath);
    }

    public void ToggleDataManual(int id) {
        currentIndex = id;
        foreach(GameObject go in fx) {
            go.SetActive(false);
        }
        if(id< fx.Length) fx[id].SetActive(true);
        //Change main tab icon.
        switch (id){ 
            case 0:
                d2Icon.sprite = mainTabIcon.sprite = drinkUsing;
                d2Icon.color = mainTabIcon.color = Color.white;
                break;
            case 1:
                d2Icon.sprite = mainTabIcon.sprite = sleepUsing;
                d2Icon.color = mainTabIcon.color = Color.white;
                break;
            case 2:
                d2Icon.sprite = mainTabIcon.sprite = bmiUsing;
                d2Icon.color = mainTabIcon.color = Color.white;
                break;
            case 3:
                d2Icon.color = mainTabIcon.color = new Color(1, 1, 1, 0);
                break;
        }
        
        LoadDataPage();
    }

    private void LoadDataPage() {
        d2Text.text = headerTxt.text = headerResultText[currentIndex];
        d2Text_data.text = dataTxt.text = dataResultText[currentIndex];
        //bullets.SetBulletId(currentIndex);

        print(currentIndex+" currentIndex");

        if(currentIndex==0){
            ProcessDrink(); 
        }else if(currentIndex==1){ 
            ProcessSleep(); 
        }else if(currentIndex==2){ 
            ProcessBodyMass();
        }else {  
            raw_ans.sprite = result_spi_List[0];
        }

    }

    public void OnClickNextData() {
        if (currentIndex < dataResultText.Length - 1) {
            currentIndex++;
            LoadDataPage();
        }
    }

    public void OnClickPrevData() {
        if (currentIndex > 0) {
            currentIndex--;
            LoadDataPage();
        }
    }

    public void OnChangeScene() {
        global.OnLoadSceneAt(1);
        Global.ins.PlaySoundIndex(0);
    }

    void ProcessDrink() {
        //drinkMass = (((global.weight * 2.2f) * 30) / 2);
        bool isEnough = false;
        if (global.age <= 8) {
            isEnough = (global.drink >= 5) ? true : false;
        } else if (global.age <= 13) {
            isEnough = (global.drink >= 7) ? true : false;
        } else if (global.age <= 18) {
            isEnough = (global.drink >= 8) ? true : false;
        } else { //over 19
            isEnough = (global.drink >= 9) ? true : false;
        }
        water.localPosition = (isEnough) ? Vector3.zero : new Vector3(0, -250, 0);
        //drinkText.text = (isEnough) ? "ปริมาณเหมาะสม" : "ปริมาณน้อยเกินไป";
        drinkUsing = drinkSpr[Convert.ToInt32(isEnough)];
print("Convert.ToInt32(isEnough) " +Convert.ToInt32(isEnough));
       raw_ans.sprite = drink_spi_List[Convert.ToInt32(isEnough)];
       // drinkIcon.sprite = drinkSpr[Convert.ToInt32(isEnough)];
        headerResultText[0] = (isEnough) ?
            "ผลการดื่มน้ำเพียงพอ<br>คุณดื่มน้ำในปริมาณที่เหมาะสม" :
            "ผลการดื่มน้ำไม่เพียงพอ<br>คุณดื่มน้ำต่อวันน้อยเกินไป";
        dataResultText[0] = (isEnough) ?
            "การดื่มน้ำให้เพียงพอมีประโยชน์มากมาย<br>" +
            "• ช่วยล้างสารพิษและขับของเสีย<br>" +
            "  ออกจากร่างกาย<br>" +
            "• บำรุงสุขภาพผิวและชะลอความแก่<br>" +
            "• ช่วยระบบย่อยอาหาร ป้องกันท้องผูก<br>" +
            "• ช่วยให้หัวใจมีอัตราการเต้นปกติ<br>" +
            "• ช่วยควบคุมอุณหภูมิร่างกาย<br>" +
            "• ช่วยการไหลเวียนโลหิต ปรับสมดุลร่างกาย<br>" +
            "• ช่วยการทำงานของสมองและมีสมาธิมากขึ้น" :

            "ผลเสียของการดื่มน้ำน้อยเกินไป<br> " +
            "• ไตทำงานหนักขึ้น<br>" +
            "• เสี่ยงต่อการติดเชื้อบริเวณทางเดินปัสสาวะ<br>" +
            "  ส่วนกลาง<br>" +
            "• มีสารก่อนิ่วที่ตกตะกอนมากกว่าปกติ<br>" +
            "• เสี่ยงต่อโรคความดันโลหิตสูง<br>" +
            "• ผิวพรรรณไม่สดใส";
    }

    void ProcessSleep() {
        bool isEnough = false;
        if (global.age <= 13) {
            isEnough = (global.sleep >= 9) ? true : false;
        } else if (global.age <= 17) {
            isEnough = (global.sleep >= 8) ? true : false;
        } else if (global.age <= 64) {
            isEnough = (global.sleep >= 7) ? true : false;
        } else { //over 19
            isEnough = (global.sleep >= 7) ? true : false;
        }
       // sleepText.text = (isEnough) ? "เหมาะสม" : "น้อยเกินไป";
       // sleepIcon.sprite = sleepSpr[Convert.ToInt32(isEnough)];
        sleepUsing = sleepSpr[Convert.ToInt32(isEnough)];

        raw_ans.sprite = sleep_spi_List[Convert.ToInt32(isEnough)];

        headerResultText[1] = (isEnough) ?
           "ผลการนอนเพียงพอ<br>คุณนอนหลับพักผ่อนเพียงพอ" :
           "ผลการนอนไม่เพียงพอ<br>คุณนอนหลับพักผ่อนไม่เพียงพอ";
        dataResultText[1] = (isEnough) ?
            "การนอนหลับที่ดี สำคัญต่อสุขภาพ การเติบโต<br>" +
            "และพัฒนาการของสมอง<br>" +
            "ระหว่างการนอน ร่างกายซ่อมแซมส่วนที่สึกหรอ<br>" +
            "ปรับสมดุลสารเคมี สมองเรียบเรียงข้อมูลและ<br>" +
            "ตลอดจนเป็นระยะที่สมองทำการเรียบเรียง<br>" +
            "จัดเก็บเป็นหมวดหมู่ ช่วยในการจดจำและ<br>"+
            "เกิดการพัฒนา<br><br>":

            "การนอนหลับไม่เพียงพอเป็น<br>" +
            "เวลานาน มีผลเสียต่อสุขภาพ<br>" +
            "สัมพันธ์กับการเกิดโรคหัวใจและ<br>" +
            "หลอดเลือด โรคเบาหวาน<br>" +
            "โรคความดันโลหิตสูง<br>" +
            "ส่งผลต่อระบบประสาท การคิด<br>" +
            "และการจำ<br><br>";

        /*dataResultText[1] += "เทคนิคที่ช่วยให้นอนหลับได้ง่ายขึ้น<br>" +
           "1) ออกกำลังกายอย่างน้อย 30 นาทีต่อวัน<br>" +
           "2) หลีกเลี่ยงอาหารมื้อหนัก และเครื่องดื่ม<br>" +
           "กระตุ้นประสาท ก่อนนอน 4 ชั่วโมง<br>" +
           "3) ผ่อนคลายร่างกาย และจิตใจก่อนนอน<br>" +
           "ด้วยการอาบน้ำอุ่น หรือการนั่งสมาธิ<br>" +
           "4) จัดระเบียบห้องนอน และกำจัดสิ่งรบกวน<br>" +
           "ด้วยการปิดไฟ และอุปกรณ์ เช่น โทรทัศน์<br>" +
           "คอมพิวเตอร์ โทรศัพท์ ก่อนนอน<br>" +
           "5) เข้านอนให้เป็นเวลา ไม่ควรนอนดึกมาก<br>" +
           "ควรเข้านอนเวลาประมาณ 21.00 - 23.00 น.<br>" +
           "และปฏิบัติให้เป็นประจำ";*/
    }

    void ProcessBodyMass() {
        float cmToM = (float)global.height / 100;
        print(cmToM + "                                                           cmToM");
        float bmi = (float)global.weight / (cmToM * cmToM);
        print(bmi + "                                                           bmiiiii");
        if (bmi <= 18.5f) {
         //   bmiText.text = "ต่ำกว่ามาตรฐาน";
            bmiImage.sprite = bmiList[0];
            //bmiIcon.sprite = bmiSpr[0];
            bmiUsing = bmiSpr[0];

            raw_ans.sprite = bmi_spi_List[0];
            headerResultText[2] = "ดัชนีมวลร่างกายต่ำกว่ามาตรฐาน";
            dataResultText[2] =
                "ส่งผลให้ร่างกายอ่อนแอ เจ็บป่วยง่าย<br>" +
                "มีประสิทธิภาพการท้างานลดลง<br>" +
                "รับประทานอาหารให้ครบ 5 หมู่<br>" +
                "เพื่อให้มีน้ำหนักเพิ่มขึ้น ออกกำลังกาย<br>" +
                "เสริมสร้างความแข็งแรงของร่างกาย";
        } else if (bmi < 23) {
         //   bmiText.text = "ปกติ";
            bmiImage.sprite = bmiList[1]; 
           //bmiIcon.sprite = bmiSpr[1];
            bmiUsing = bmiSpr[1];
            raw_ans.sprite = bmi_spi_List[1];
            headerResultText[2] = "ดัชนีมวลร่างกายปกติ";
            dataResultText[2] =
                "รักษาสุขภาพให้แข็งแรงเสมอ<br>" +
                "เลือกทานอาหารที่มีคุณค่า<br>" +
                "ออกกกำลังกายให้เหมาะสมกับ<br>" +
                "สภาพร่างกาย";
        } else if (bmi < 25) {
         //   bmiText.text = "สูงกว่ามาตรฐาน";
            bmiImage.sprite = bmiList[2];
            //bmiIcon.sprite = bmiSpr[2];
            raw_ans.sprite = bmi_spi_List[2];
            bmiUsing = bmiSpr[2];
            headerResultText[2] = "ดัชนีมวลร่างกายสูงกว่ามาตรฐาน";
            dataResultText[2] =
                "เสี่ยงต่อการเป็นโรคอ้วน<br>" +
                "ลดการบริโภคไขมันและพลังงาน<br>" +
                "เพิ่มการออกก้าลังกายให้เหมาะสม<br>" +
                "กับสภาพร่างกาย";
        } else { //over 25 - 29.9
        //    bmiText.text = "สูงกว่ามาตรฐานมาก";
            bmiImage.sprite = bmiList[3];
            //bmiIcon.sprite = bmiSpr[3];
            bmiUsing = bmiSpr[3];
            raw_ans.sprite = bmi_spi_List[3];
            headerResultText[2] = "ค่าดัชนีมวลกาย เกินกว่ามาตรฐานมาก";
            dataResultText[2] =
                "ส่งผลให้ระดับโคเลสเตอรอลและ<br>" +
                "ไตรกลีเซอไรด์สูง เสี่ยงกับการเกิด<br>" +
                "โรคหัวใจ เบาหวาน ความดันโลหิตสูง<br>" +
                "ข้อเข่าเสื่อม<br>" +
                "ควรลดการบริโภคไขมันและพลังงาน<br>" +
                "เพิ่มการออกกำลังกายที่ไม่มีแรงกระแทก<br>" +
                "และใช้อุปกรณ์ที่ช่วยลดการกระแทก";
        }
    }

    void ProcessResult() {
        headerResultText[3] = "สรุป";
        dataResultText[3] = "หากคุณมีการกิน เรียนหรือทำงาน<br>"+
                             "ที่แข็งแรง ไม่เจ็บป่วย<br>"+
                             "พักผ่อน นอนหลับ และออกกำลังกาย<br>"+
                             "อย่างสมดุล จะช่วยให้แข็งแรง<br>"+
                             "ไม่เจ็บป่วย และมีสุขภาพดีในระยะยาว";
    }

    float timer = 0;
    private void Update() {
        if (Input.GetMouseButton(0)) {
            timer = 0;
        }
        timer += Time.deltaTime;
        if (timer >= Global.ins.interval) {
            Global.ins.OnLoadSceneAt(1);
        }
    }




    Sprite LoadNewSprite(string FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight) {

        // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference

        Texture2D SpriteTexture = LoadTexture(FilePath);
        Sprite NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit, 0, spriteType);

        return NewSprite;
    }

    Sprite ConvertTextureToSprite(Texture2D texture, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.Tight) {
        // Converts a Texture2D to a sprite, assign this texture to a new sprite and return its reference

        Sprite NewSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), PixelsPerUnit, 0, spriteType);

        return NewSprite;
    }

    Texture2D LoadTexture(string FilePath) {

        // Load a PNG or JPG file from disk to a Texture2D
        // Returns null if load fails

        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath)) {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);           // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
                return Tex2D;                 // If data = readable -> return texture
        }
        return null;                     // Return null if load failed
    }

}
